package software.libre;
/**
 * librerias usadas para la ventana
 */
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * 
 * @author kevingodoy
 */
public class GestionMensaje {
/**
 * Crea como atributos privados las listas de la clase de el paquete modelo
 */
    private List<Mensaje> mensajes;

/**
 * primer constructor en el cual crea listas vacias 
 */
    public GestionMensaje() {
        mensajes = new ArrayList<Mensaje>();
        
        
    }

    public void loadCliente(String para, String asunto, String mensaje) {
        Mensaje c = new Mensaje();
        c.setPara(para);
        c.setAsunto(asunto);
        c.setMensaje(mensaje);
        mensajes.add(c);

    }
/**
 * atributo publico lista de clientes 
 * @return 
 */
    public List<Mensaje> getCliente() {
        return mensajes;
    }
 /**
     * Metodo para escribir archivo (Objeto)
     */
     public void EscribirArchivo(String para, String asuntos, String mens) {
        Mensaje c = new Mensaje();
        c.setPara(para);
        c.setAsunto(asuntos);
        c.setMensaje(mens);
        mensajes.add(c);
        try {
            String ruta = "Software.txt";
            FileOutputStream file = new FileOutputStream(ruta);
            ObjectOutputStream escritura = new ObjectOutputStream(file);

            escritura.writeObject(mensajes);

            escritura.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        }

    }

    /**
     * Metodo para leer un archivo de texto(objeto)
     *
     * @return lista de personas
     */
    public List<Mensaje> leerArchivo() {

        FileInputStream archivoLectura = null;
        ObjectInputStream entrada = null;
        try {
            String ruta = "Software.txt";
            archivoLectura = new FileInputStream(ruta);
            entrada = new ObjectInputStream(archivoLectura);
            List<Mensaje> cli = (List<Mensaje>) entrada.readObject();
            this.mensajes = cli;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mensajes;
    }

    
    
    
    
    
}
           
       
    


