package software.libre;
/**
 * librerias usadas para la ventana 
 */
//import controlador.GestionBiblioteca;
import java.util.ArrayList;
import java.util.List;

import javax.swing.table.AbstractTableModel;

//import modelo.Cliente;


/**
 * 
 * @author kevingodoy
 */

/**
 * 
 * Registro de los clientes con su respectiva herencia
 */
public class TablaCorreos extends AbstractTableModel {
    /**
     * atributos privados 
     */
    private GestionMensaje gb;
	 public String[] columnas = {"Para", "Asunto","Mensaje"};

	    public Class[] columnasTipos = { String.class, String.class, String.class};

	    private List<Mensaje> datos;
/**
 * primer constructor 
 */
	    public TablaCorreos() {
	      super();
	      datos = new ArrayList<Mensaje>();
	    }
	    /**
             * Segundo constructor en el cual manda como parametros a la lista clientes 
             * @param datos 
             */
	    public TablaCorreos(List<Mensaje> datos) {
	      super();
	      this.datos = datos;
	    }
              
/**
 * colunmas de la tabla
 * @return 
 */
	public int getColumnCount() {
		// TODO Auto-generated method stub
		return columnas.length;
	}

	
	public int getRowCount() {
		// TODO Auto-generated method stub
		return datos.size();
	}

	/***
         * Metodo en el cual setea los atributos de la clase 
         * @param value
         * @param row
         * @param colum 
         */
	public void setValueAt(Object value, int row, int colum) {
		Mensaje dat = (Mensaje)(datos.get(row));
		/**
                 * switch en el cual manda los datos 
                 */
		  switch (colum) {
	      case 0:
	        dat.setPara((String) value);
	        break;
	      case 1:
	        dat.setAsunto((String) value);
	        break;
	      case 2:
	    	  dat.setMensaje((String) value);
	      }
	    }
	    public String getColumnName(int colum) {
	      return columnas[colum];
	    }

	    public Class getColumnClass(int colum) {
	      return columnasTipos[colum];
	    }
	    public Object getValueAt(int row, int colum) {
	      Mensaje dat = (Mensaje) (datos.get(row));
	      /**
               * switch en el cual obtiene los datos 
               */
	      switch (colum) {
	      case 0:
	        return dat.getPara();
	      case 1:
	        return dat.getAsunto();
	      case 2:
	    	  return dat.getMensaje();
	  
	      }
	      return new String();
	}

    void columnas(String string) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
