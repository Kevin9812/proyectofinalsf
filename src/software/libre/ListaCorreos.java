package software.libre;
/**
 * librerias usadas para la ventana
 */
import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.*;

import javax.swing.*;

import java.util.List;


/**
 * 
 * @author kevingodoy
 */

/**
 * Registro de los clientes con su respectiva herencia e interfaz
 */
public class ListaCorreos extends JFrame implements ActionListener  {
 private JTable tablaMensajes;
	 /**
          * variables globales privadas como JTextFields y JButtons,ademas de la clase Gestion Biblioteca
          */
	 
	
	 private JButton cmdListar;
	 private JButton cmdSalir;
	 private GestionMensaje gb;
	 /**
          * Primer constructor que contiene como parametro a la clase del controlador Gestion Biblioteca y que dentro de ella manda las caracteristicas que va a tener la ventana
          * @param gb 
          */
	public ListaCorreos(GestionMensaje gb) {
		this.gb = gb;
		this.setSize(600,500);
                this.setTitle("Correos Enviados");
              
		//metodo donde se encuentran todos los componentes de la ventana
		initComponents();
           
	}
        
        public ListaCorreos() {
	
	
	}
    
    /**
     * void que contiene todos los elementos a usar en nuestra ventana
     */
	public void initComponents() {
		
	  setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	  //Obtiene un contenedor compuesto por un gridLayout de 1x1
          getContentPane().setLayout(new GridLayout(0, 1));
      
         /**
          * crea una nueva tabla en donde le setea los datos de la clase TablaCliente
          */
	 tablaMensajes= new JTable();
         tablaMensajes.setModel(new TablaCorreos());
         
 
        JScrollPane scrollPaneTabla = new JScrollPane(tablaMensajes);
   
		/**
                 * Crea JButtons en donde el addActionListener indica que boton va a cumplir una accion y el setActionCommand
                 * es el que le asigna el nombre a ese boton
                 */
		
		cmdListar = new JButton("Listar");
		cmdListar.addActionListener(this);
		cmdListar.setActionCommand("btnListar");
		
		
		cmdSalir = new JButton("Salir");
		cmdSalir.addActionListener(this);
		cmdSalir.setActionCommand("btnSalir");
                /**
                 * crea un panel
                 */
		JPanel p = new JPanel();
		/**
                 * setea como un nuevo FlowLayout
                 */
		p.setLayout(new FlowLayout());
		/**
                 * crea borde de panel
                 */
		p.setBorder(BorderFactory.createTitledBorder("Listado de Correos"));
		/**
                 * obtiene el contenedor y anade el panel
                 */
		getContentPane().add(p);
	        /**
                 * anade labels y TexFields al panel 
                 */
		
		/**
                 * anade botones y los alinea 
                 */
		p.add(cmdListar, BorderLayout.SOUTH);
	
		p.add(cmdSalir, BorderLayout.SOUTH);
		/**
                 * anade la tabla al panel
                 */
		getContentPane().add(scrollPaneTabla, BorderLayout.SOUTH);

	}
	/**
         *devuelve un String en el cual me devuelve las acciones que toma cada uno de los elementos creados en el panel
         * @param e 
         */
	@Override
	//acciones
	public void actionPerformed(ActionEvent e) {//evt
		// TODO Auto-generated method stub
        String op = e.getActionCommand();
		
		System.out.println("comando " + op);
		/**
                 * switch en el cual se encuentran los metodos a realizar cuando el usiario le de un click a cualquiera de los elementos creados
                 */
		switch (op) {
		
		case "btnListar":
			listar();
			break;
	
                case "btnSalir":
			salir();
			break;
		default:
			break;
		}
		
	}
    
	
        /**
	 * metodo listar de la clase RegistroClientes
	 */
	private void listar() {
		// TODO Auto-generated method stub
               List<Mensaje>mensajes= gb.getCliente();
                        for(Mensaje cli : mensajes){
                               System.out.println((cli.getPara()+"\n"+cli.getAsunto()+"\n"+cli.getMensaje()));
                            
                          }
                          /**
                           * llamamos al archivo de texto y lo listamos en la tabla 
                           */
                          tablaMensajes.setModel(new TablaCorreos(gb.leerArchivo()));
                              
                              
        }
        
             /**
              * metodo para salir de la ventana
              */
	private void salir() {
		// TODO Auto-generated method stub
		this.dispose();
	}

	    
	
	
}
	
	
	
	
	
	
	
	
	


